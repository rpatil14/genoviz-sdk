/**
 * Copyright (c) 2007 Affymetrix, Inc.
 * 
* Licensed under the Common Public License, Version 1.0 (the "License"). A copy
 * of the license must be included with any distribution of this source code.
 * Distributions from Affymetrix, Inc., place this in the IGB_LICENSE.html file.
 * 
* The license is also available at http://www.opensource.org/licenses/cpl.php
 */
package com.affymetrix.genoviz.glyph;

import com.affymetrix.genoviz.bioviews.ViewI;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;

/**
 * A glyph that is drawn as a painted rectangle.
 */
public final class EfficientPaintRectGlyph extends EfficientSolidGlyph {

    private Paint paint = new GradientPaint(0, 0, Color.GREEN, 5, 2, Color.YELLOW, true);

    public void setPaint(Paint p) {
        this.paint = p;
    }

    @Override
    public void draw(ViewI view) {
        Rectangle pixelbox = view.getScratchPixBox();
        view.transformToPixels(this.getCoordBox(), pixelbox);

        Graphics2D g = view.getGraphics();

        optimizeBigRectangleRendering(view, pixelbox);

        pixelbox.width = Math.max(pixelbox.width, getMinPixelsWidth());
        pixelbox.height = Math.max(pixelbox.height, getMinPixelsHeight());

        // draw the box
        g.setPaint(paint);
        g.fillRect(pixelbox.x, pixelbox.y, pixelbox.width, pixelbox.height);

        super.draw(view);
    }

}
